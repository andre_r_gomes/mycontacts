//
//  MCBaseViewController.swift
//  MyContacts
//
//  Created by Andre Gomes on 12/10/16.
//  Copyright © 2016 Andre Gomes. All rights reserved.
//

import UIKit
import CoreData

class MCBaseViewController: UIViewController {

    var dataService: MCDataService?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //applyNavigationBarStyling()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.dataService = appDelegate.dataService
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Helpers
    func applyNavigationBarStyling() {
        let backgroundColor = UIColor.green
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController!.navigationBar.tintColor = backgroundColor
        self.navigationController!.navigationBar.barTintColor = backgroundColor
        self.navigationController!.navigationBar.backgroundColor = backgroundColor
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
