//
//  MCListViewController+UIViewControllerPreviewing.swift
//  MyContacts
//
//  Created by Andre Gomes on 12/11/16.
//  Copyright © 2016 Andre Gomes. All rights reserved.
//

import UIKit

extension MCListViewController: UIViewControllerPreviewingDelegate {
    
    /// Create a previewing view controller to be shown at "Peek".
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        // check if we're not already displaying a preview controller (WebViewController is my preview controller)
        if (self.presentedViewController is MCAddViewController) {
            return nil;
        }
        
        let cellPostion = self.view.convert(location, to: self.tableView)
        guard let indexPath = self.tableView.indexPathForRow(at: cellPostion) else { return nil }
        guard let tableCell = self.tableView.cellForRow(at: indexPath) else { return nil }

        previewingContext.sourceRect = self.tableView.convert(tableCell.frame, to: self.view)
        
        let record = fetchedResultsController.object(at: indexPath as IndexPath) as! ContactItem
        let viewController = MCContactPeekPopPreviewViewController(nibName: "MCContactPeekPopPreviewViewController", bundle: nil)
        viewController.contactItem = record;
        
        return viewController
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        // if you want to present the selected view controller as it self us this:
        // [self presentViewController:viewControllerToCommit animated:YES completion:nil];
        // to render it with a navigation controller (more common) you should use this:
        
        //self.navigationController!.show(viewControllerToCommit, sender: nil)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "MCAddViewController") as! MCAddViewController
        viewController.contactItem = (viewControllerToCommit as! MCContactPeekPopPreviewViewController).contactItem
        
        self.navigationController!.show(viewController, sender: nil)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if self.isForceTouchAvailable() {
            if !(self.previewingContext != nil) {
                self.previewingContext = self.registerForPreviewing(with: self, sourceView: self.view)
            }
        }
        else {
            if (self.previewingContext != nil) {
                self.unregisterForPreviewing(withContext: self.previewingContext!)
                self.previewingContext = nil
            }
        }
    }
    
    
    // MARK: -
    // MARK: Helper Methods
    func isForceTouchAvailable() -> Bool {
        var isForceTouchAvailable = false
        if (self.traitCollection.responds(to: #selector(getter: UITraitCollection.forceTouchCapability))) {
            isForceTouchAvailable = self.traitCollection.forceTouchCapability == .available
        }
        return isForceTouchAvailable
    }
}
