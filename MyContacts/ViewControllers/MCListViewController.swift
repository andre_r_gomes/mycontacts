//
//  MCListViewController.swift
//  MyContacts
//
//  Created by Andre Gomes on 12/10/16.
//  Copyright © 2016 Andre Gomes. All rights reserved.
//

import UIKit
import CoreData
import MessageUI

class MCListViewController: MCBaseViewController,
    UITableViewDataSource,
    UITableViewDelegate,
    NSFetchedResultsControllerDelegate,
    MFMailComposeViewControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addContactButton: UIButton!
    public var context: NSManagedObjectContext!
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
    var previewingContext: UIViewControllerPreviewing?

    
    // MARK: -
    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Contacts"

        // Subscribe for change notifications
        NotificationCenter.default.addObserver(self, selector: #selector(MCListViewController.handleModelChange(_:)), name: NSNotification.Name(rawValue: "MyContactsModelChangedNotification"), object: nil)
        
        // TableView Setup
        registerNibs()
        configureTableView()
        configureAddContactButton()
        configureFetchedResultsController()
        
        
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.userInfo)")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: -
    // MARK: Fetched Results Controller Delegate Methods
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
         self.tableView.endUpdates()
    }
  
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.tableView.insertRows(at: [indexPath as IndexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath as IndexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                let cell = self.tableView.cellForRow(at: indexPath as IndexPath) as! MCContactTableViewCell
                configureCell(cell: cell, atIndexPath: indexPath as NSIndexPath)
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath as IndexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                self.tableView.insertRows(at: [newIndexPath as IndexPath], with: .fade)
            }
            break;
        }
    }
    
    
    // MARK: -
    // MARK: UITableViewDataSource Methods
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if let sections = fetchedResultsController.sections {
            return sections.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultsController.sections {
            let sectionInfo = sections[section]
            return sectionInfo.numberOfObjects
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MCContactTableViewCell", for: indexPath) as! MCContactTableViewCell
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        
        // Configure Table View Cell
        configureCell(cell: cell, atIndexPath: indexPath as NSIndexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            let record = fetchedResultsController.object(at: indexPath as IndexPath) as! ContactItem
            self.dataService?.deleteContectItem(contactItem: record)
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let record = fetchedResultsController.object(at: indexPath as IndexPath) as! ContactItem
        
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            self.dataService?.deleteContectItem(contactItem: record)
        }
        delete.backgroundColor = UIColor.red
        
        
        var array : [UITableViewRowAction] = [delete]
        
        
        if ((record.email?.characters.count)! > 0) {
            let email = UITableViewRowAction(style: .normal, title: "Email") { action, index in
                if MFMailComposeViewController.canSendMail() {
                    let mail = MFMailComposeViewController()
                    mail.mailComposeDelegate = self
                    mail.setSubject("")
                    mail.setMessageBody("", isHTML: false)
                    mail.setToRecipients([record.email!])
                    self.present(mail, animated: true, completion: { _ in })
                }
                else {
                    let alertController = UIAlertController(title: "Error", message: "You do not have a mail account setup on this device" as String, preferredStyle: .alert)
                    
                    let cancelAction = UIAlertAction(title: "OK", style: .cancel) { action in
                        // ...
                    }
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true) {
                        // ...
                    }
                }
            }
            email.backgroundColor = UIColor.blue
            
            array.append(email)
        }
        
        
        if ((record.phoneNumber?.characters.count)! > 0) {
            let call = UITableViewRowAction(style: .normal, title: "Call") { action, index in
                let urlString = String(format: "tel://%@", record.phoneNumber!)
                let url = URL(string: urlString)!
                if UIApplication.shared.canOpenURL(url) {
                    if UIApplication.shared.responds(to: #selector(UIApplication.open(_:options:completionHandler:))) {
                        UIApplication.shared.open(url, options: [:], completionHandler: {(_ success: Bool) -> Void in
                        })
                    }
                    else {
                        UIApplication.shared.openURL(url)
                    }
                }
            }
            call.backgroundColor = UIColor.orange
            
            array.append(call)
        }
        
        return array
    }
    
    
    // MARK: -
    // MARK: UITableViewDataSource Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        let record = fetchedResultsController.object(at: indexPath as IndexPath) as! ContactItem
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "MCAddViewController") as! MCAddViewController
        viewController.contactItem = record
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    // MARK: -
    // MARK: Private Methods
    func registerNibs() {
        self.tableView.register(UINib(nibName: "MCContactTableViewCell", bundle: nil), forCellReuseIdentifier: "MCContactTableViewCell")
    }
    
    func configureTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 100
        // This will remove extra separators from tableview
        //self.tableView.tableFooterView! = UIView(frame: CGRect.zero)
    }
    
    func configureFetchedResultsController() {
        // FetchedResultsController Setup
        self.context = self.dataService?.managedObjectContext
        
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ContactItem")
        
        // Add Sort Descriptors
        let sortDescriptor = NSSortDescriptor(key: "createdDate", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Initialize Fetched Results Controller
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.context, sectionNameKeyPath: nil, cacheName: nil)
        
        // Configure Fetched Results Controller
        self.fetchedResultsController.delegate = self
    }
    
    func configureAddContactButton() {
        self.addContactButton.backgroundColor = UIColor.blue
        self.addContactButton.addTarget(self, action: #selector(MCListViewController.addContactButtonHandler), for: .touchUpInside)
        self.addContactButton.layer.cornerRadius = 10
        self.addContactButton.setTitle("Add Contact", for: .normal)
    }
    
    func configureCell(cell: MCContactTableViewCell, atIndexPath indexPath: NSIndexPath) {
        // Fetch Record
        let record = fetchedResultsController.object(at: indexPath as IndexPath) as! ContactItem
        
        cell.decorate(contactItem: record)
    }
    
    
    // MARK: -
    // MARK: Button Handler
    func addContactButtonHandler() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "MCAddViewController") as! MCAddViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    // MARK: -
    // MARK: MFMailComposeViewControllerDelegate Methods
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: { _ in })
    }

    
    // MARK: -
    // MARK: Notification Handler
    func handleModelChange(_ notification: Notification) {
        
    }
}

