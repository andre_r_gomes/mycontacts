//
//  MCAddViewController.swift
//  MyContacts
//
//  Created by Andre Gomes on 12/10/16.
//  Copyright © 2016 Andre Gomes. All rights reserved.
//

import UIKit
import MobileCoreServices
import Photos

class MCAddViewController: MCBaseViewController,
    UITextFieldDelegate,
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate,
    UIPickerViewDataSource,
    UIPickerViewDelegate {

    var contactItem: ContactItem?
    @IBOutlet weak var displayImageButton: UIButton!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var colorPickerTextField: UITextField!
    @IBOutlet weak var actionButton: UIButton!
    
    var colorPickerView: UIPickerView!
    var pickerColorCollection = [UIColor.lightGray, UIColor.red, UIColor.green, UIColor.blue, UIColor.yellow, UIColor.orange, UIColor.purple]
    
    // MARK: -
    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureDisplayImageButton()
        configureActionButton()
        configureTextFields()
        configureColorPicker()
        
        if (self.contactItem != nil) {
            self.fullNameTextField.text = self.contactItem?.fullName
            self.phoneNumberTextField.text = self.contactItem?.phoneNumber
            self.emailTextField.text = self.contactItem?.email
            
            if self.contactItem?.color != nil {
                let backgroundColor = NSKeyedUnarchiver.unarchiveObject(with: self.contactItem?.color as! Data) as? UIColor
                self.colorPickerTextField.backgroundColor = backgroundColor
                self.displayImageButton.layer.borderColor = backgroundColor?.cgColor
            } else {
                self.displayImageButton.layer.borderColor = UIColor.lightGray.cgColor
            }
            
            if self.contactItem?.photo != nil {
                let profileImage = NSKeyedUnarchiver.unarchiveObject(with: self.contactItem?.photo as! Data) as? UIImage
                self.displayImageButton.setImage(profileImage, for: .normal)
                self.displayImageButton.setTitle("", for: .normal)
            }
            
            self.title = "Edit Contact"
            self.actionButton.setTitle("Update", for: UIControlState.normal)
        } else {
            self.title = "New Contact"
            self.actionButton.setTitle("Save", for: UIControlState.normal)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: -
    // MARK: helper Methods
    func configureDisplayImageButton() {
        self.displayImageButton.layer.cornerRadius = self.displayImageButton.frame.size.width / 2
        self.displayImageButton.addTarget(self, action: #selector(MCAddViewController.displayImageButtonHandler), for: .touchUpInside)
        self.displayImageButton.clipsToBounds = true
        self.displayImageButton.imageView?.contentMode = UIViewContentMode.scaleAspectFill
        self.displayImageButton.setTitle("Add Photo", for: .normal)
        self.displayImageButton.setTitleColor(UIColor.black, for: .normal)
        self.displayImageButton.layer.borderColor = UIColor.lightGray.cgColor
        self.displayImageButton.layer.borderWidth = 2
        self.displayImageButton.setImage(UIImage(), for: .normal)
    }
    
    func configureActionButton() {
        self.actionButton.backgroundColor = UIColor.blue
        self.actionButton.addTarget(self, action: #selector(MCAddViewController.actionButtonHandler), for: .touchUpInside)
        self.actionButton.layer.cornerRadius = 10
    }
    
    func configureTextFields() {
        self.fullNameTextField.delegate = self
        self.phoneNumberTextField.delegate = self
        self.emailTextField.delegate = self
        self.colorPickerTextField.delegate = self
        
        
        self.fullNameTextField.placeholder = "Full Name"
        self.phoneNumberTextField.placeholder = "Phone Number"
        self.emailTextField.placeholder = "Email"
        self.colorPickerTextField.text = "Select a color"
    }
    
    func configureColorPicker() {
        self.colorPickerView = UIPickerView()
        self.colorPickerView.showsSelectionIndicator = true
        self.colorPickerView.dataSource = self;
        self.colorPickerView.delegate = self;
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(MCAddViewController.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.colorPickerTextField.inputView = self.colorPickerView
        self.colorPickerTextField.inputAccessoryView = toolBar
    }
    
    func canProceedWithSave() -> Bool {
        
        var errorMessage: String = ""
        
        if ((self.fullNameTextField.text?.characters.count)! <= 0) {
            errorMessage = "Please insert a valid full name."
        }
        else if ((self.phoneNumberTextField.text?.characters.count)! < 7) {
            errorMessage = "Please insert a valid phone number at least 7 digits long."
        }
        else if ((self.fullNameTextField.text?.characters.count)! > 0 && isEmailValid(self.emailTextField.text!) == false) {
            errorMessage = "Please insert a valid email address."
        }
        
        
        if (errorMessage.characters.count > 0) {
            
            let alertController = UIAlertController(title: "Error", message: errorMessage as String, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "OK", style: .cancel) { action in
                // ...
            }
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true) {
                // ...
            }

            return false
        }
        
        
        return true
    }
    
    func isEmailValid(_ email: String) -> Bool {
        let emailRegex = "^\\w+([-+.\']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$"
        let test = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        
        if email.characters.count > 0 {
            return test.evaluate(with: email)
        }
        return false
    }
    
    func presentImagePickerControlelr() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = .photoLibrary
            imagePickerController.mediaTypes = [kUTTypeImage as String]
            imagePickerController.allowsEditing = true
            imagePickerController.modalPresentationStyle = .popover
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    func presentFailedLibraryPermissionsAlertDialog() {
        let alertController = UIAlertController(title: "Permission Denied", message: "Adding a photo requires permission to access your library." as String, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "OK", style: .cancel) { action in
            // ...
        }
        alertController.addAction(cancelAction)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { action in
            let url = URL(string: UIApplicationOpenSettingsURLString)!
            if UIApplication.shared.canOpenURL(url) {
                if UIApplication.shared.responds(to: #selector(UIApplication.open(_:options:completionHandler:))) {
                    UIApplication.shared.open(url, options: [:], completionHandler: {(_ success: Bool) -> Void in
                    })
                }
                else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        alertController.addAction(settingsAction)
        
        
        self.present(alertController, animated: true) {
            // ...
        }
    }
    
    
    // MARK: -
    // MARK: Button Handlers
    func displayImageButtonHandler() {
        
        let authorizationStatus = PHPhotoLibrary.authorizationStatus() as PHAuthorizationStatus
    
        if authorizationStatus == .authorized {
            self.presentImagePickerControlelr()
        } else if authorizationStatus == .notDetermined {
            
            PHPhotoLibrary.requestAuthorization({(status:PHAuthorizationStatus) in
                DispatchQueue.main.async {
                    switch status{
                        case .authorized:
                            self.presentImagePickerControlelr()
                            break
                        case .restricted:
                            self.presentFailedLibraryPermissionsAlertDialog()
                            break
                        case .denied:
                            self.presentFailedLibraryPermissionsAlertDialog()
                            break
                        default:
                            print("Default")
                            break
                    }
                }
            })
            
        } else {
            self.presentFailedLibraryPermissionsAlertDialog()
        }
        
            
            /*
            let library = ALAssetsLibrary()
            library.enumerateGroupsWithTypes(.All, usingBlock: { (group, stop) -> Void in
                // User clicked ok
            }, failureBlock: { (error) -> Void in
                // User clicked don't allow
                imagePickerController.dismissViewControllerAnimated(true, completion: nil)
            })
 */
        
    }
    
    func actionButtonHandler() {
        if (self.contactItem == nil) {
            self.contactItem = self.dataService?.createContactItem()
        }
        
        
        // Negative Cases
        if canProceedWithSave() == false {
            return
        }
        
        self.contactItem?.modifiedDate = NSDate()
        self.contactItem?.fullName = self.fullNameTextField.text
        self.contactItem?.phoneNumber = self.phoneNumberTextField.text
        self.contactItem?.email = self.emailTextField.text
        
        if (self.displayImageButton.currentImage != nil) {
            let photoData = NSKeyedArchiver.archivedData(withRootObject: self.displayImageButton.currentImage as Any)
            self.contactItem?.photo = photoData as NSData?
        }
        
        let colorData = NSKeyedArchiver.archivedData(withRootObject: self.colorPickerTextField.backgroundColor as Any)
        self.contactItem?.color = colorData as NSData?
        
        self.dataService?.saveContext()
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func donePicker () {
        self.colorPickerTextField.resignFirstResponder()
    }
    
    
    // MARK: -
    // MARK: UITextFieldDelegate Methods
    public func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField == self.fullNameTextField {
//            self.contactItem?.fullName = textField.text
//        } else if textField == self.phoneNumberTextField {
//            self.contactItem?.phoneNumber = textField.text
//        }else if textField == self.emailTextField {
//            self.contactItem?.email = textField.text
//        }
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        return false;
    }
    
  
    // MARK: -
    // MARK: UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let dict = info as NSDictionary
        
        let croppedImage = dict.object(forKey: UIImagePickerControllerEditedImage)
        let image = dict.object(forKey: UIImagePickerControllerOriginalImage)
        
        if croppedImage != nil {
            self.displayImageButton.setImage(croppedImage as! UIImage?, for: .normal)
        } else if image != nil {
            self.displayImageButton.setImage(image as! UIImage?, for: .normal)
        }
        
        self.dismiss(animated: true, completion: nil);
    }
    
    
    // MARK: -
    // MARK: UIPickerViewDataSource Methods
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerColorCollection.count;
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let view = UIView()
        view.backgroundColor = self.pickerColorCollection[row]
        return view
    }
    
    
    // MARK: -
    // MARK: UIPickerViewDelegate Methods
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.colorPickerTextField.backgroundColor = self.pickerColorCollection[row]
        self.displayImageButton.layer.borderColor = self.pickerColorCollection[row].cgColor
    }
}
