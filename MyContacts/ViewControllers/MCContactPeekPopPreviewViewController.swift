//
//  MCContactPeekPopPreviewViewController.swift
//  MyContacts
//
//  Created by Andre Gomes on 12/15/16.
//  Copyright © 2016 Andre Gomes. All rights reserved.
//

import UIKit

class MCContactPeekPopPreviewViewController: UIViewController {

    var contactItem: ContactItem?
    @IBOutlet weak var displayImageImageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.displayImageImageView.layer.cornerRadius = self.displayImageImageView.frame.size.width / 2
        self.displayImageImageView.clipsToBounds = true
        self.displayImageImageView?.contentMode = UIViewContentMode.scaleAspectFill
        self.displayImageImageView.layer.borderColor = UIColor.gray.cgColor
        self.displayImageImageView.layer.borderWidth = 2
        
        self.fullNameLabel.text = self.contactItem?.fullName
        
        if self.contactItem?.color != nil {
            let backgroundColor = NSKeyedUnarchiver.unarchiveObject(with: self.contactItem?.color as! Data) as? UIColor
            self.fullNameLabel.backgroundColor = backgroundColor
            self.displayImageImageView.layer.borderColor = backgroundColor?.cgColor
        }
        
        if self.contactItem?.photo != nil {
            let profileImage = NSKeyedUnarchiver.unarchiveObject(with: self.contactItem?.photo as! Data) as? UIImage
            self.displayImageImageView.image = profileImage
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
