//
//  MCLandingViewController.swift
//  MyContacts
//
//  Created by Andre Gomes on 12/10/16.
//  Copyright © 2016 Andre Gomes. All rights reserved.
//

import UIKit

class MCLandingViewController: MCBaseViewController {

    @IBOutlet weak var contactsButton: UIButton!
    @IBOutlet weak var addContactButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Welcome"
        
        self.contactsButton.backgroundColor = UIColor.blue
        self.contactsButton.setTitle("Contacts", for: .normal)
        self.contactsButton.addTarget(self, action: #selector(MCLandingViewController.contactListButtonHandler), for: .touchUpInside)
        self.contactsButton.layer.cornerRadius = 10
        
        self.addContactButton.backgroundColor = UIColor.blue
        self.addContactButton.setTitle("Add Contact", for: .normal)
        self.addContactButton.addTarget(self, action: #selector(MCLandingViewController.addContactListButtonHandler), for: .touchUpInside)
        self.addContactButton.layer.cornerRadius = 10
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func contactListButtonHandler() {
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let viewController = storyBoard.instantiateViewController(withIdentifier: "MCListViewController") as! MCListViewController
        let viewController = MCListViewController(nibName: "MCListViewController", bundle: nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func addContactListButtonHandler() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "MCAddViewController") as! MCAddViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
