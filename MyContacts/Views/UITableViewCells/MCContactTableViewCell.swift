//
//  MCContactTableViewCell.swift
//  MyContacts
//
//  Created by Andre Gomes on 12/10/16.
//  Copyright © 2016 Andre Gomes. All rights reserved.
//

import UIKit

class MCContactTableViewCell: UITableViewCell {
    
    @IBOutlet weak var displayImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.displayImageView.layer.cornerRadius = self.displayImageView.frame.size.width / 2
        self.displayImageView.clipsToBounds = true
        self.displayImageView?.contentMode = UIViewContentMode.scaleAspectFill
        self.displayImageView.layer.borderWidth = 2
    }

    func decorate( contactItem: ContactItem) {
        
        self.titleLabel.text = contactItem.fullName
        self.subTitleLabel.text = contactItem.phoneNumber
        
        if contactItem.color != nil {
            let backgroundColor = NSKeyedUnarchiver.unarchiveObject(with: contactItem.color as! Data) as? UIColor
            self.displayImageView.layer.borderColor = backgroundColor?.cgColor
        } else {
            self.displayImageView.layer.borderColor = UIColor.gray.cgColor
        }
        
        if contactItem.photo != nil {
            let profileImage = NSKeyedUnarchiver.unarchiveObject(with: contactItem.photo as! Data) as? UIImage
            self.displayImageView.image = profileImage
//            self.displayImageView.layer.borderColor = UIColor.clear.cgColor
//            self.displayImageView.layer.borderWidth = 0
        }
    }
}
