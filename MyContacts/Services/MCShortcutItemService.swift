//
//  MCShortcutItemService.swift
//  MyContacts
//
//  Created by Andre Gomes on 12/14/16.
//  Copyright © 2016 Andre Gomes. All rights reserved.
//

import UIKit

struct MCShortcutItemServiceConstants{
    static let contactListIdentifier = "ContactsList"
    static let addContactIdentifier = "AddContact"
}

class MCShortcutItemService: NSObject {

    // MARK: -
    // MARK: Lifecycle Methods
    override init() {
        super.init()
        configureShortcutItems()
    }
    
    func configureShortcutItems() {
        if UIScreen.main.traitCollection.responds(to: #selector(getter: UITraitCollection.forceTouchCapability)) {
            if UIScreen.main.traitCollection.forceTouchCapability == .available {
                var shortcutItems = [UIApplicationShortcutItem]()
                shortcutItems.append(addContactShortcutItem())
                shortcutItems.append(contactListShortcutItem())
                UIApplication.shared.shortcutItems = shortcutItems
            }
        }
    }
    
    
    // MARK: -
    // MARK: Private Methods
    private func addContactShortcutItem() -> UIApplicationShortcutItem {
        let identifier: String = (Bundle.main.bundleIdentifier?.appendingFormat(".%@", MCShortcutItemServiceConstants.addContactIdentifier))!
        let addContactShortcutItem = UIApplicationShortcutItem(type: identifier, localizedTitle: "Add Contact", localizedSubtitle: nil, icon: UIApplicationShortcutIcon(type: .compose), userInfo: shortcutItemUserInfo() as? [AnyHashable : Any])
        return addContactShortcutItem
    }
    
    private func contactListShortcutItem() -> UIApplicationShortcutItem {
        let identifier: String = (Bundle.main.bundleIdentifier?.appendingFormat(".%@", MCShortcutItemServiceConstants.contactListIdentifier))!
        let contactListShortcutItem = UIApplicationShortcutItem(type: identifier, localizedTitle: "Contacts List", localizedSubtitle: nil, icon: UIApplicationShortcutIcon(type: .contact), userInfo: shortcutItemUserInfo() as? [AnyHashable : Any])
        return contactListShortcutItem
    }
    
    // MARK: -
    // MARK: Helper Methods
    private func shortcutItemUserInfo() -> NSDictionary {
        return ["version": Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString")!]
    }
}
