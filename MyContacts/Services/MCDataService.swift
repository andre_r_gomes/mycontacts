//
//  MCDataService.swift
//  MyContacts
//
//  Created by Andre Gomes on 12/14/16.
//  Copyright © 2016 Andre Gomes. All rights reserved.
//

import UIKit
import CoreData

class MCDataService: NSObject {
    
    // var managedObjectContext: NSManagedObjectContext?

    init(usingManagedObjectContext context: NSManagedObjectContext) {
        super.init()
        self.managedObjectContext = context
    }
    
    override init() {
        super.init()
        //self.managedObjectContext = self.persistentContainer.viewContext
    }
    
    func retrieveContactItem(usingFullName name: NSString) -> ContactItem {
        var array: NSArray?
        
        if (name.length > 0) {
            let predicate = NSPredicate(format: "fullName == %@", name)

            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ContactItem")
            request.predicate = predicate
            
            do {
                array = try self.managedObjectContext?.fetch(request) as NSArray?
            }
            catch {
            }
        }
        return array?.firstObject as! ContactItem
    }
    
    func retrieveAllContactItems() -> NSArray {
        var array: NSArray?
        let request = ContactItem.contactItemsFetchRequest()
        do {
            array = try self.managedObjectContext?.fetch(request) as NSArray?
        }
        catch {
        }
        return array!
    }
    
    func createContactItem() -> ContactItem {
        let contactItem = (NSEntityDescription.insertNewObject(forEntityName: "ContactItem", into: self.managedObjectContext!) as? ContactItem)!
        contactItem.createdDate = NSDate()
      return contactItem
    }
    
    func deleteContectItem(contactItem: ContactItem) {
        self.managedObjectContext?.delete(contactItem)
        self.saveContext()
    }
    
    /*
    func saveContext() {
        do {
            try self.managedObjectContext?.save()
        } catch {
            // Handle error stored in *error* here
        }
    }
    */
    
    
    
    // MARK: - Core Data stack
    
    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        var managedObjectContext = NSManagedObjectContext(concurrencyType:.mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator
        return managedObjectContext
    }()
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.AnyTap.Swift_Widget" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1] as NSURL
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "MyContacts", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        
        let directory = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.com.demo.MyContacts")
        
        let url = directory?.appendingPathComponent("MyContacts.sqlite")
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        
        
        do {
            try coordinator?.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        return coordinator
    }()
    
    
    /*
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "MyContacts")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    */
    
    // MARK: - Core Data Saving support
    func saveContext () {
        if (self.managedObjectContext?.hasChanges)! {
            do {
                try self.managedObjectContext?.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
