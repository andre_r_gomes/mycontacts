//
//  AppDelegate.swift
//  MyContacts
//
//  Created by Andre Gomes on 12/10/16.
//  Copyright © 2016 Andre Gomes. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navigationController: UINavigationController?
    var dataService: MCDataService?
    var shortcutItemService: MCShortcutItemService?

    // MARK: - Lifecycle Methods
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        self.window = application.windows.first;
        self.navigationController = window?.rootViewController as! UINavigationController?
        
        self.dataService = MCDataService()
        self.shortcutItemService = MCShortcutItemService.init()
        return handleLaunchOptions(launchOptions)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        // Check if widget changed data..
        let sharedDefaults = UserDefaults(suiteName: "group.com.demo.MyContacts")!
        
        if sharedDefaults.bool(forKey: "MyContactsModelChanged") {
            sharedDefaults.removeObject(forKey: "MyContactsModelChanged")
            sharedDefaults.synchronize()
            NotificationCenter.default.post(name: Notification.Name(rawValue: "MyContactsModelChangedNotification"), object: nil)
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.dataService?.saveContext()
    }

    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (_ succeeded: Bool) -> Void) {
        self.handle(shortcutItem)
    }
    
    // MARK: - Helper Methods -
    func handleLaunchOptions(_ launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        if (launchOptions != nil) {

            let shortcutItem = (launchOptions?[UIApplicationLaunchOptionsKey.shortcutItem] as? UIApplicationShortcutItem)
            if  let _:AnyObject = shortcutItem {
                self.handle(shortcutItem!)
                return false
            }
        }
        return true
    }
    
    func handle(_ shortcutItem: UIApplicationShortcutItem) {
        self.navigationController!.dismiss(animated: false, completion: { _ in })
        self.navigationController!.popToRootViewController(animated: false)
        
        let addContactIdentifier: String = (Bundle.main.bundleIdentifier?.appendingFormat(".%@", MCShortcutItemServiceConstants.addContactIdentifier))!
        if (shortcutItem.type == addContactIdentifier) {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: "MCAddViewController") as! MCAddViewController
            self.navigationController?.pushViewController(viewController, animated: true)
            return
        }
        
        let contactListIdentifier: String = (Bundle.main.bundleIdentifier?.appendingFormat(".%@", MCShortcutItemServiceConstants.contactListIdentifier))!
        if (shortcutItem.type == contactListIdentifier) {
            let viewController = MCListViewController(nibName: "MCListViewController", bundle: nil)
            self.navigationController?.pushViewController(viewController, animated: true)
            return
        }
    }
}

