//
//  ContactItem+CoreDataProperties.swift
//  MyContacts
//
//  Created by Andre Gomes on 12/10/16.
//  Copyright © 2016 Andre Gomes. All rights reserved.
//

import Foundation
import CoreData


extension ContactItem {

    @nonobjc public class func contactItemsFetchRequest() -> NSFetchRequest<ContactItem> {
        return NSFetchRequest<ContactItem>(entityName: "ContactItem");
    }

    @NSManaged public var fullName: String?
    @NSManaged public var phoneNumber: String?
    @NSManaged public var email: String?
    @NSManaged public var color: NSData?
    @NSManaged public var photo: NSData?
    @NSManaged public var createdDate: NSDate?
    @NSManaged public var modifiedDate: NSDate?
    
}
