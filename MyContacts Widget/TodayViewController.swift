//
//  TodayViewController.swift
//  MyContacts Widget
//
//  Created by Andre Gomes on 12/15/16.
//  Copyright © 2016 Andre Gomes. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding {
        
    @IBOutlet weak var displayImageView: UIImageView!
    @IBOutlet weak var displayLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add the iOS 10 Show More ability
        self.extensionContext!.widgetLargestAvailableDisplayMode = .expanded
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        let dataService = MCDataService()
        let array = dataService.retrieveAllContactItems() as NSArray
        let contactItem = array.firstObject as? ContactItem
        
        if (contactItem != nil) {
            self.displayImageView.layer.cornerRadius = self.displayImageView.frame.size.width / 2
            self.displayImageView.clipsToBounds = true
            self.displayImageView?.contentMode = UIViewContentMode.scaleAspectFill
            self.displayImageView.layer.borderWidth = 2
            
            self.displayLabel.text = contactItem?.fullName
            
            if contactItem?.color != nil {
                let backgroundColor = NSKeyedUnarchiver.unarchiveObject(with: contactItem?.color as! Data) as? UIColor
                self.displayImageView.layer.borderColor = backgroundColor?.cgColor
            } else {
                self.displayImageView.layer.borderColor = UIColor.lightGray.cgColor
            }
            
            if contactItem?.photo != nil {
                let profileImage = NSKeyedUnarchiver.unarchiveObject(with: contactItem?.photo as! Data) as? UIImage
                self.displayImageView.image = profileImage
            }
        } else {
            self.displayLabel.text = "No Contacts"
        }
        
        // Save in shared user defaults
        let sharedDefaults = UserDefaults(suiteName: "group.AnyTap.SwiftWidget")!
        sharedDefaults.set(true, forKey: "SwiftWidgetModelChanged")
        sharedDefaults.synchronize()
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        completionHandler(NCUpdateResult.newData)
    }
    
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        if activeDisplayMode == .expanded { // compact
            // Changed to compact mode
            self.preferredContentSize = maxSize
        }
        else {
            // Changed to expanded mode
            self.preferredContentSize = CGSize(width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height + 25))
        }
    }
    
}
